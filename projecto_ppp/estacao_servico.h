//
//  estacao_servico.h
//  projecto_ppp
//
//  Created by Pedro Quitério on 28/04/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#ifndef projecto_ppp_estacao_servico_h
#define projecto_ppp_estacao_servico_h

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#include "time_functions.h"
//#include "Files.h"
//#include "menu.h"

#define MAX_NOME 51
#define MAX_DATA 20

typedef int bool;
#define TRUE 0
#define FALSE 1

typedef struct cliente {
    struct cliente * anterior;
    char nome[MAX_NOME];
    int numero_cliente;
    int nif;
    struct cliente * seguinte;
} Cliente;

typedef struct reserva {
    struct reserva * anterior;
    Cliente * cliente;
    char data[MAX_DATA];            //  dd/mm/aaaa/hh/mm
    int tempo;
    struct reserva * seguinte;
}Reserva;

typedef struct pre_reserva {
    struct pre_reserva * anterior;
    Cliente * cliente;
    char data[MAX_DATA];            //  dd/mm/aaaa/hh/mm
    char data_insercao[MAX_DATA];
    int tempo;
    struct pre_reserva * seguinte;
}Pre_reserva;

Reserva * nova_reserva();
//Reserva * cria_reserva(Reserva *, Cliente *, char *, int);
void cria_reserva(Reserva *, Cliente *, char *, int);
//void insere_reserva(Reserva *, Pre_reserva *, Cliente *, char *, int);
Reserva * procura_reserva_sobreposta(Reserva *, char *, int);
Reserva * procura_reserva_data(Reserva *, char *);
Reserva * cancela_reserva(Reserva *, Cliente *,char *);
Reserva * procura_reserva_antiga(Reserva *, char *);
Reserva * cancela_reservas_antigas(Reserva *, Cliente *);
void imprime_reservas(Reserva *, Pre_reserva *, int);
void available_block(Reserva *, char *, int);
void free_reservas(Reserva *);


Pre_reserva * nova_pre_reserva();
//Pre_reserva * cria_pre_reserva(Pre_reserva *, Cliente *, char *, int);
void cria_pre_reserva(Pre_reserva *, Cliente *, char *, int);
void add_pre_reserva_from_file(Pre_reserva *, Cliente *, int, char *, char *, int);
Pre_reserva * cancela_pre_reserva(Pre_reserva *, Cliente *, char *, char *, int);
Pre_reserva * procura_pre_reserva_antiga(Pre_reserva *, char *);
Pre_reserva * cancela_pre_reservas_antigas(Pre_reserva *, Cliente *);
Pre_reserva * check_pre_reserva(Reserva *, Pre_reserva *);
Pre_reserva * procura_pre_reserva(Pre_reserva *, int, char *, char *, int);
Pre_reserva * procura_pre_reserva_2(Pre_reserva *, int, char *, int);
void free_pre_reservas(Pre_reserva *);

Cliente * novo_cliente();
int cria_cliente(Cliente *, char *, int);
void add_cliente_from_file(Cliente *, char *, int, int);
Cliente * procura_nif(Cliente *, int);
Cliente * procura_numero_cliente(Cliente *, int);
Cliente * procura_cliente_nome(Cliente *, char *);
void imprime_reservas_cliente(Cliente *, Reserva *, Pre_reserva *, int);
void imprime_cliente_especifico(Cliente *);
Cliente * last_client(Cliente *);
int num_reservas_cliente(Reserva *, Pre_reserva *, int, int);
void free_clientes(Cliente *);

#endif



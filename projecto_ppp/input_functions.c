//
//  input_functions.c
//  projecto_ppp
//
//  Created by Pedro Quitério on 16/05/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#include <stdio.h>

#include "input_functions.h"

int get_inteiro(void) {
    int inteiro;
    char buffer[200];
    fgets(buffer, 200, stdin);
    fseek(stdin, SEEK_END, 0);
    buffer[strlen(buffer)-1] = '\0';
    inteiro = atoi(buffer);
    if (inteiro == 0 && strcmp(buffer, "0") != 0) inteiro = -1;
    return inteiro;
}

int get_nif(void) {
    int nif;
    printf("Insira o NIF: ");
    nif = get_inteiro();
    if (nif < 100000000 || nif > 999999999) {
        do {
            printf("NIF inválido. Tem que conter 9 dígitos!\n");
            printf("Insira o NIF: ");
            nif = get_inteiro();
        } while ((nif < 100000000 || nif > 999999999));
    }
    return nif;
}

bool get_confirmation(void) {
    char buffer[200];
    fgets(buffer, 200, stdin);
    fseek(stdin, SEEK_END, 0);
    buffer[strlen(buffer)-1] = '\0';
    int i = 0;
    while (i < strlen(buffer)) {
        buffer[i] = tolower(buffer[i]);
        i++;
    }
    if (strcmp(buffer, "s") == 0 || strcmp(buffer, "sim") == 0 || strcmp(buffer,"y") == 0 || strcmp(buffer, "yes") == 0) return TRUE;
    else if (strcmp(buffer, "n") == 0 || strcmp(buffer, "não") == 0 || strcmp(buffer, "nao") == 0) return FALSE;
    do {
        printf("Caracteres inválidos! Opções disponíveis: [S/N]");
        fgets(buffer, 200, stdin);
        fseek(stdin, SEEK_END, 0);
        buffer[strlen(buffer)] = '\0';
        int i = 0;
        while (i < strlen(buffer)) {
            buffer[i] = tolower(buffer[i]);   /* Pomos o buffer em minusculas para ser mais facil comparar */
            i++;
        }
        if (strcmp(buffer, "s") == 0 || strcmp(buffer, "sim") == 0) return TRUE;
        else if (strcmp(buffer, "n") == 0 || strcmp(buffer, "não") == 0 || strcmp(buffer, "nao")) return FALSE;
    } while (1);
}










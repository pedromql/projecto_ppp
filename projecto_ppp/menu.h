//
//  menu.h
//  projecto_ppp
//
//  Created by Pedro Quitério on 16/05/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#ifndef projecto_ppp_menu_h
#define projecto_ppp_menu_h

#include "estacao_servico.h"
#include "input_functions.h"
#include "utils.h"
#include "Files.h"

int menu(Reserva *, Pre_reserva *, Cliente *);
void car(int);
void intro(void);
int menu_inicial(Reserva *, Pre_reserva *, Cliente *);
void menu_criar_cliente(Cliente *);
void menu_efectuar_reserva(Reserva *, Pre_reserva *, Cliente *);
Reserva * menu_cancelar_reserva(Pre_reserva *, Reserva *,Cliente *);
Pre_reserva * menu_cancelar_pre_reserva(Pre_reserva *,Reserva *, Cliente *);
void menu_listar_reservas(Reserva *, Pre_reserva *, Cliente *);

#endif

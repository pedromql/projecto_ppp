//
//  main.c
//  projecto_ppp
//
//  Created by Pedro Quitério on 28/04/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#include <stdio.h>
#include "main.h"


int main(int argc, const char * argv[])
{
    Reserva * reservas = nova_reserva();
    Pre_reserva * pre_reservas = nova_pre_reserva();
    Cliente * clientes = novo_cliente();
    menu(reservas,pre_reservas,clientes);
    
    printf("\n********************************\n*                              *\n* Pelo menos corre até ao fim  *\n*                              *\n********************************\n\n");
    return 0;
}


//
//  utils.c
//  projecto_ppp
//
//  Created by Pedro Quitério on 17/05/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#include <stdio.h>


#include "utils.h"

bool check_numbers_in_string(char * string) {
    int i = 0;
    while (i < strlen(string)) {
        if (isdigit(string[i]) != 0) return TRUE;
        i++;
    }
    return FALSE;
}
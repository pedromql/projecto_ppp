//
//  time_functions.h
//  projecto_ppp
//
//  Created by Pedro Quitério on 08/05/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#ifndef projecto_ppp_time_functions_h
#define projecto_ppp_time_functions_h

#include "estacao_servico.h"

struct tm parse_time_to_struct(char *, time_t *);
double time_difference(char *, char *);
char * time_to_string();
int check_data(char *);
int check_data_2(char *, int);
int check_last_block(char *, int);
int check_data_bounds(char *);
void AddTime(int, struct tm *);
void imprime_data_conclusao(char *, int);
void beginning_of_day(char *, char *);
void last_block_of_day(char *, char *);
void next_block(char *, char *);

#endif

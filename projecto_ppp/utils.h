//
//  utils.h
//  projecto_ppp
//
//  Created by Pedro Quitério on 17/05/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#ifndef projecto_ppp_utils_h
#define projecto_ppp_utils_h

#include "estacao_servico.h"

bool check_numbers_in_string(char *);

#endif

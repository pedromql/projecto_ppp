//
//  input_functions.h
//  projecto_ppp
//
//  Created by Pedro Quitério on 16/05/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#ifndef projecto_ppp_input_functions_h
#define projecto_ppp_input_functions_h

#include "estacao_servico.h"

int get_inteiro(void);
int get_nif(void);
bool get_confirmation(void);

#endif

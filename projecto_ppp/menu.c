//
//  menu.c
//  projecto_ppp
//
//  Created by Pedro Quitério on 16/05/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#include <stdio.h>

#include "menu.h"

int menu(Reserva * reservas, Pre_reserva * pre_reservas, Cliente * clientes)
{
    intro();
    //cria_cliente(clientes, "Pedro", 123123123);
    //cria_cliente(clientes, "Cenas", 212345678);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 08:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 09:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 10:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 11:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 12:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 13:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 14:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 15:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 16:00",60);
//    insere_reserva(reservas, pre_reservas, clientes, "12/12/2014 17:00",60);
    menu_inicial(reservas, pre_reservas, clientes);
    printf("Prima enter para terminar!\n");
    getchar();
    return 0;
}

void car(int white_spaces) {
    int i = 0;
    while (i < white_spaces){
        i++;
        printf(" ");
    }
    printf("      .--------.\n");
    i = 0;
    while (i < white_spaces){
        i++;
        printf(" ");
    }
    printf(" ____/_____|___ \\___\n");
    i = 0;
    while (i < white_spaces){
        i++;
        printf(" ");
    }
    printf("O    _   - |   _   ,*\n");
    i = 0;
    while (i < white_spaces){
        i++;
        printf(" ");
    }
    printf(" '--(_)-------(_)--'\n");
}

void intro(void) {
    printf("Bem vindo ao sistema de gestão da estação de serviço!\n\n");
    car(15);
    printf("\nPrima enter para continuar!\n");
    getchar();
    //system("clear");
    //printf("\033c");
    fseek(stdin, SEEK_END, 0);
}

int menu_inicial(Reserva * reservas, Pre_reserva * pre_reservas, Cliente * clientes)
{
    readClientFromFile(clientes);
    readReservFromFile(reservas, clientes);
    readPreReservFromFile(pre_reservas, clientes);
    
    reservas->seguinte->anterior = reservas;
    clientes->seguinte->anterior = clientes;
    
    reservas = cancela_reservas_antigas(reservas, clientes);
    pre_reservas = cancela_pre_reservas_antigas(pre_reservas, clientes);
    int opcao;
    do {
        printf("1 - Criar cliente;\n2 - Efectuar reserva;\n3 - Cancelar reserva;\n4 - Cancelar pré-reserva;\n5 - Listar;\n0 - Sair;\n");
        printf("Opção: ");
        opcao = get_inteiro();
        if (opcao < 0 || opcao > 5) {
            do {
                printf("Opcão inválida!\n");
                printf("1 - Criar cliente;\n2 - Efectuar reserva;\n4 - Cancelar reserva;\n4 - Cancelar pré-reserva;\n5 - Listar;\n0 - Sair;\n");
                printf("Opção: ");
                opcao = get_inteiro();
            } while (opcao < 0 || opcao > 5);
        }
        switch (opcao) {
            case 1: menu_criar_cliente(clientes);
                break;
            case 2: menu_efectuar_reserva(reservas, pre_reservas, clientes);
                break;
            case 3: reservas = menu_cancelar_reserva(pre_reservas,reservas,clientes);
                pre_reservas = check_pre_reserva(reservas, pre_reservas);
                break;
            case 4: pre_reservas = menu_cancelar_pre_reserva(pre_reservas,reservas, clientes);
                break;
            case 5: menu_listar_reservas(reservas,pre_reservas,clientes);
                break;
        }
        
    } while (opcao != 0);
    
    // Quando acabar de correr o programa guardamos todas as reservas e pre reservas feitas.
    writeClientToFile(clientes);
    writeReservToFile(reservas);
    writePreResToFile(pre_reservas);
    
    free_reservas(reservas);
    free_pre_reservas(pre_reservas);
    free_clientes(clientes);
    
    return 0;
}

void menu_criar_cliente(Cliente * clientes) {
    char buffer[200];
    char nome[MAX_NOME];
    int nif;
    printf("Insira o nome do cliente: ");
    fgets(buffer, 200, stdin);
    buffer[strlen(buffer)-1] = '\0';
    fseek(stdin, SEEK_END, 0);
    if (strlen(buffer) > 50 || strlen(buffer) < 2 || check_numbers_in_string(buffer) == TRUE) {
        do {
            if (strlen(buffer) > 50) printf("Demasiados caracteres. 50 caracteres no máximo!\n");
            if (strlen(buffer) < 2) printf("Caracteres Insuficientes!\n");
            if (check_numbers_in_string(buffer) == TRUE) printf("Dígitos não são aceites no nome!\n");
            printf("Insira o nome do cliente: ");
            fgets(buffer, 200, stdin);
            fseek(stdin, SEEK_END, 0);
            buffer[strlen(buffer)-1] = '\0';
        } while (strlen(buffer) > 50 || strlen(buffer) < 2 || check_numbers_in_string(buffer) == TRUE);
    }
    strcpy(nome, buffer);
    nif = get_nif();
    Cliente * cliente_temp = procura_nif(clientes, nif);
    if (cliente_temp != NULL) {
        do {
            printf("NIF já existente. Pertence ao cliente: \n");
            imprime_cliente_especifico(cliente_temp);
            nif = get_nif();
            cliente_temp = procura_nif(clientes, nif);
        } while (cliente_temp != NULL);
    }
    printf("Número de cliente atribuído: %d\n",cria_cliente(clientes, nome, nif));  // Numero de Cliente
}


void menu_efectuar_reserva(Reserva * reservas, Pre_reserva * pre_reservas, Cliente * clientes) {
    Cliente * cliente_temp;
    int numero_cliente, tempo, opcao;
    char data[20];
    printf("1 - Lavagem;\n2 - Manutenção;\nOpção: ");
    opcao = get_inteiro();
    if (opcao != 1 && opcao != 2) {
        do {
            printf("1 - Lavagem;\n2 - Manutenção;\nOpção: ");
            opcao = get_inteiro();
        } while (opcao != 1 && opcao != 2);
    }
    if (opcao == 1) tempo = 30;
    else tempo = 60;
    printf("Insira o número do cliente: ");
    numero_cliente = get_inteiro();
    cliente_temp = procura_numero_cliente(clientes, numero_cliente);
    if (numero_cliente <= 0 || procura_numero_cliente(clientes, numero_cliente) == NULL) {
        do {
            if (numero_cliente <= 0) printf("Número inválido!\n");
            else if (procura_numero_cliente(clientes, numero_cliente) == NULL) {
                printf("Cliente não encontrado! Pretende criar um novo cliente? [S/N]\n");
                if (get_confirmation() == TRUE) {
                    menu_criar_cliente(clientes);
                    cliente_temp = last_client(clientes);
                    break;
                }
                else return; //acho que é melhor para nao ficar preso aqui caso nao queria criar
            }
            printf("Insira o número do cliente: ");
            numero_cliente = get_inteiro();
            cliente_temp = procura_numero_cliente(clientes, numero_cliente);
        } while (numero_cliente <= 0 || procura_numero_cliente(clientes, numero_cliente) == NULL);
    }
    while (1) {
        printf("Insira a data da reserva [dd/mm/aaaa hh:mm]: ");
        fgets(data, 20, stdin);
        fseek(stdin, SEEK_END, 0);
        data[strlen(data)-1] = '\0';
        int data_checker = check_data(data);
        int data_checker_2 = check_data_2(data, tempo);
        int last_block = check_last_block(data, tempo);
        if (data_checker != 0 || last_block != 0 || data_checker_2 != 0) {
            do {
                if (data_checker == -1) printf("Data inválida!\n");
                else if (data_checker == -2) printf("Formato inválido!\n");
                else if (data_checker == -3) printf("O serviço tem que começar à hora ou meia hora certa!\n");
                else if (data_checker == -4) printf("A estação de serviço só funciona das 8h às 18h!\n");
                else if (data_checker == -5) printf("A reserva não pode ser anterior ao tempo actual!\n");
                else if (data_checker == -6) printf("Apenas pode reservar no espaço de um ano!\n");
                else if (data_checker_2 == -1) printf("A estação de serviço encontra-se encerrada ao Sábado a partir das 13h e ao Domingo!\n");
                else if (last_block == -1) printf("Uma manutenção não pode ser marcada para o último bloco horário!\n");
                printf("Insira a data da reserva [dd/mm/aaaa hh:mm]: ");
                fgets(data, 20, stdin);
                fseek(stdin, SEEK_END, 0);
                data[strlen(data)-1] = '\0';
                data_checker = check_data(data);
                data_checker_2 = check_data_2(data, tempo);
                last_block = check_last_block(data, tempo);
            } while (data_checker != 0 || last_block != 0 || data_checker_2 != 0);
        }
        if (procura_reserva_sobreposta(reservas, data, tempo) == NULL) {
            printf("A reserva será efectuada:\n");
            imprime_cliente_especifico(cliente_temp);
            printf("Data: %s\n",data);
            printf("Data de conclusão: ");
            imprime_data_conclusao(data,tempo);
            printf("Tem a certeza que pretende fazer a reserva? [S/N]\n");
            if (get_confirmation() == TRUE) {
                cria_reserva(reservas, cliente_temp, data, tempo);
                printf("Reserva criada com sucesso!\n");
                
                
            }
            else printf("Reserva cancelada!\n");
            car(3);
            printf("\nPrima enter para continuar!\n");
            getchar();
            fseek(stdin, SEEK_END, 0);
            return;
        }
        else {
            printf("Bloco já ocupado!\n");
            available_block(reservas, data, tempo);
            printf("1 - Efectuar pré-reserva para a data inserida;\n2 - Alterar data;\n3 - Cancelar;\nOpção: ");
            opcao = get_inteiro();
            if (opcao != 1 && opcao != 2 && opcao != 3) {
                do {
                    printf("1 - Efectuar pré-reserva para a data inserida;\n2 - Alterar data;\n3 - Cancelar;\nOpção: ");
                    opcao = get_inteiro();
                } while (opcao != 1 && opcao != 2 && opcao != 3);
            }
            if (opcao == 1) {
                if (procura_pre_reserva_2(pre_reservas, cliente_temp->numero_cliente, data, tempo) != NULL) {
                    printf("Já tem uma pre-reserva com esses dados registada!\n");
                }
                else {
                    cria_pre_reserva(pre_reservas, cliente_temp, data, tempo);
                    printf("Pré-reserva criada com sucesso!\n");
                    car(5);
                    printf("\nPrima enter para continuar!\n");
                    getchar();
                    fseek(stdin, SEEK_END, 0);
                    return;
                }
            }
            else if (opcao == 3) {
                printf("Cancelado!\n");
                car(0);
                printf("\nPrima enter para continuar!\n");
                getchar();
                fseek(stdin, SEEK_END, 0);
                return;
            }
        }
    }
}

Reserva * menu_cancelar_reserva(Pre_reserva * pre_reservas,Reserva * reservas , Cliente *clientes)
{
    /* Pocurar por numero de cliente*/
    //Cliente *cliente_temp;
    int numero_cliente;
    int data_checker;
    char data[20];
    printf("Insira o numero de cliente: "); // LEL
    numero_cliente = get_inteiro();
    //cliente_temp = procura_numero_cliente(clientes, numero_cliente);
    if (numero_cliente <= 0 || procura_numero_cliente(clientes, numero_cliente) == NULL)
    {
        do {
            if (numero_cliente <= 0) printf("Número inválido!\n");
            else if (procura_numero_cliente(clientes, numero_cliente) == NULL) {
                printf("Cliente não encontrado!\n");
            }
            printf("Insira o número do cliente: ");
            numero_cliente = get_inteiro();
            //cliente_temp = procura_numero_cliente(clientes, numero_cliente);
        } while (numero_cliente <= 0 || procura_numero_cliente(clientes, numero_cliente) == NULL);
    }
    
    //Listar as reservas do Cliente , escolher a q vamos cancelar
    if ( num_reservas_cliente(reservas, pre_reservas, numero_cliente, 1) == 0 )
    {
        printf("Não tem reservas.\n");
        return reservas;
    }
    else
    {
        printf("As suas reservas: \n");
        imprime_reservas_cliente(procura_numero_cliente(clientes, numero_cliente), reservas, pre_reservas, 0);
        printf("\n");
    }
    // Cancelar as reservas
    if (reservas->cliente != NULL)
    {
        printf("Insira a data da reserva que pretende cancelar [dd/mm/aaaa hh:mm]: ");
        fgets(data,20,stdin);
        fseek(stdin, SEEK_END, 0);
        data[strlen(data)-1] = '\0';
        data_checker = check_data(data);
        if ( data_checker != 0)
        {
            do
            {
                if (data_checker == -1) printf("Data inválida!\n");
                else if (data_checker == -2) printf("Formato inválido!\n");
                printf("Insira a data da reserva que pretende cancelar [dd/mm/aaaa hh:mm]: ");
                fgets(data, 20, stdin);
                fseek(stdin, SEEK_END, 0);
                data[strlen(data)-1] = '\0';
                data_checker = check_data(data);
            }while (data_checker!= 0);
        }
        if (procura_reserva_data(reservas, data)->cliente->numero_cliente != numero_cliente) {
            printf("A reserva indicada não pertence ao cliente especificado!\n");
            return reservas;
        }
        reservas = cancela_reserva(reservas,procura_reserva_data(reservas, data)->cliente, data);
        printf("Reserva Cancelada!\n");
        return reservas;
        //pre_reservas = check_pre_reserva(reservas, pre_reservas);
    }

    return reservas;
    
}

Pre_reserva * menu_cancelar_pre_reserva(Pre_reserva * pre_reservas ,Reserva * reservas, Cliente * clientes)
{
    //Cliente *cliente_temp;
    int numero_cliente, tempo;
    char data[20];
    //char data_insercao[20];
    printf("Insira o numero de cliente: ");
    numero_cliente = get_inteiro();
    //cliente_temp = procura_numero_cliente(clientes, numero_cliente);
    if (numero_cliente <= 0 || procura_numero_cliente(clientes, numero_cliente) == NULL)
    {
        do {
            if (numero_cliente <= 0) printf("Número inválido!\n");
            else if (procura_numero_cliente(clientes, numero_cliente) == NULL) {
                printf("Cliente não encontrado! Pretende criar um novo cliente? [S/N]\n");
                if (get_confirmation() == TRUE) {
                    menu_criar_cliente(clientes);
                    //cliente_temp = last_client(clientes);
                    break;
                }
            }
            printf("Insira o número do cliente: ");
            numero_cliente = get_inteiro();
            //cliente_temp = procura_numero_cliente(clientes, numero_cliente);
        } while (numero_cliente <= 0 || procura_numero_cliente(clientes, numero_cliente) == NULL);
    }
    
    // Listar as pre-reservas do Cliente
    if ( num_reservas_cliente(reservas, pre_reservas, numero_cliente, 2) == 0 )
    {
        printf("Não tem pre-reservas.\n");
        return pre_reservas;
    }
    printf("As suas Pre-Reservas: ");
    imprime_reservas_cliente(procura_numero_cliente(clientes, numero_cliente), reservas, pre_reservas, 1);
    printf("\n");
    
    // Cancelar as Pre-Reservas
    printf("Insira a data da Pre-Reserva que pretende cancelar [dd/mm/aaaa hh:mm]: ");
    fgets(data,20,stdin);
    fseek(stdin, SEEK_END, 0);
    data[strlen(data)-1] = '\0';
    int data_checker;
    //int data_checker2;
    data_checker = check_data(data);
    if ( data_checker != 0)
    {
        do
        {
            if (data_checker == -1) printf("Data inválida!\n");
            else if (data_checker == -2) printf("Formato inválido!\n");
            printf("Insira a data da pre-reserva que pretende cancelar [dd/mm/aaaa hh:mm]: ");
            fgets(data, 20, stdin);
            fseek(stdin, SEEK_END, 0);
            data[strlen(data)-1] = '\0';
            data_checker = check_data(data);
        }while (data_checker!= 0);
    }
//    printf("Insira a data de inserção da Pre-Reserva: ");
//    fgets(data_insercao,20,stdin);
//    fseek(stdin, SEEK_END, 0);
//    data_insercao[strlen(data_insercao)-1] = '\0';
//    data_checker2 = check_data(data);
//    if ( data_checker2 != 0)
//    {
//        do
//        {
//            if (data_checker2 == -1) printf("Data inválida!\n");
//            else if (data_checker2 == -2) printf("Formato inválido!\n");
//            printf("Insira a data da reserva [dd/mm/aaaa hh:mm]: ");
//            fgets(data, 20, stdin);
//            fseek(stdin, SEEK_END, 0);
//            data[strlen(data)-1] = '\0';
//            data_checker2 = check_data(data);
//        }while (data_checker2!= 0);
//    }
    printf("Insira o tempo de duração da Pre-Reserva: ");
    tempo = get_inteiro();
    if (tempo != 30 && tempo != 60) {
        do {
            printf("Tempo inválido! Insira o tempo de duração da Pre-Reserva: ");
            tempo = get_inteiro();
        } while (tempo != 30 && tempo != 60);
    }
    Pre_reserva * pre_reserva_a_cancelar = procura_pre_reserva_2(pre_reservas,numero_cliente, data, /*data_insercao,*/ tempo);
    if (pre_reserva_a_cancelar == NULL) {
        printf("Não existe nenhuma pre-reserva com esse dados!\n");
        return pre_reservas;
    }
        //Protect
    pre_reservas = cancela_pre_reserva(pre_reservas, procura_numero_cliente(clientes, numero_cliente), data, pre_reserva_a_cancelar->data_insercao, tempo);
    printf("Pre-reserva cancelada!\n");
    return pre_reservas;
    // All good!
}

void menu_listar_reservas(Reserva * reservas, Pre_reserva * pre_reservas, Cliente * clientes)
{
    //Listar as reservas ordenadas por data , antigas primeiro
    //Listar as reservas e as pre reservas dum cliente , as mais recentes primeiros
    int opcao;
    int numeroCliente;
    printf("1 - Listar Reservas\n2 - Listar reservas e pré-reservas de um cliente.\n");
    opcao = get_inteiro();
    if ( opcao != 1 && opcao != 2 )
    {
        do
        {
            printf("1 - Listar Reservas\n2 - Listar reservas e pré-reservas de um cliente.\n");
            opcao = get_inteiro();
        } while (opcao != 1 && opcao != 2);
    }
    if ( opcao == 1)
    {
        printf("As reservas inseridas até ao momento.\n");
        imprime_reservas(reservas, pre_reservas, 2);
        
    }
    if ( opcao == 2 )
    {
        printf("Insira o numero de cliente: ");
        numeroCliente = get_inteiro();
        printf("1 - Imprimir reservas;\n2 - Imprimir pre-reservas;\nOpção: ");
        opcao = get_inteiro()-1;
        //imprime_cliente_especifico(procura_numero_cliente(clientes, numeroCliente));
        imprime_reservas_cliente(procura_numero_cliente(clientes, numeroCliente), reservas, pre_reservas, 2);
    }
    printf("\n");
}






//
//  time_functions.c
//  projecto_ppp
//
//  Created by Pedro Quitério on 08/05/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#include "time_functions.h"

struct tm parse_time_to_struct(char * time1, time_t * now) {
    struct tm time2;
    time2 = *localtime(now);
    char * timex = (char*)malloc(17*sizeof(char)); //strtok only working with this
    strcpy(timex, time1);
    char * temp;
    temp = strtok(timex, "/ :");
    time2.tm_mday = atoi(temp);
    temp = strtok(NULL, "/ :");
    time2.tm_mon = atoi(temp)-1;
    temp = strtok(NULL, "/ :");
    time2.tm_year = atoi(temp)-1900;
    temp = strtok(NULL, "/ :");
    time2.tm_hour = atoi(temp);
    temp = strtok(NULL, "/ :");
    time2.tm_min = atoi(temp);
    free(timex);
    return time2;
}


//printf("%f",time_difference("03/05/2014/17/36", "08/05/2014/17/36")); using this function
double time_difference(char * time_1, char * time_2) {
    time_t now;
    time(&now); //ir buscar tempo apenas uma vez para garantir que os segundos são os mesmos
    struct tm time1 = parse_time_to_struct(time_1,&now);
    struct tm time2 = parse_time_to_struct(time_2,&now);
    return difftime(mktime(&time1), mktime(&time2));
}

char * time_to_string() {
    time_t now;
    time(&now);
    struct tm temp;
    temp = *localtime(&now);
    mktime(&temp); //this line will update the day of week
    char * data = (char *)malloc(20*sizeof(char));
    data[0] = '\0';
    char buffer[18];
    if (temp.tm_mday < 10) strcat(data,"0");
    sprintf(buffer,"%d/",temp.tm_mday);
    strcat(data,buffer);
    if (temp.tm_mon+1 < 10) strcat(data,"0");
    sprintf(buffer,"%d/",temp.tm_mon+1);
    strcat(data,buffer);
    sprintf(buffer,"%d ",temp.tm_year+1900);
    strcat(data,buffer);
    if (temp.tm_hour < 10) strcat(data,"0");
    sprintf(buffer,"%d:",temp.tm_hour);
    strcat(data,buffer);
    if (temp.tm_min < 10) strcat(data,"0");
    sprintf(buffer,"%d",temp.tm_min);
    strcat(data,buffer);
    return data; //have to free this pointer after copied
}

int check_data(char * data) { //dd/mm/aaaa hh:mm
    if (strlen(data) != 16 || check_data_bounds(data) == -1) return -1; //data invalida
    if (isdigit(data[0]) == 0 || isdigit(data[1]) == 0 || data[2] != '/' || isdigit(data[3]) == 0 || isdigit(data[4]) == 0 || data[5] != '/' || isdigit(data[6]) == 0 || isdigit(data[7]) == 0 || isdigit(data[8]) == 0 || isdigit(data[9]) == 0 || data[10] != ' ' || isdigit(data[11]) == 0 || isdigit(data[12]) == 0 || data[13] != ':' || isdigit(data[14]) == 0 || isdigit(data[15]) == 0) return -2; //formato inválido
    if (data[15] != '0' || (data[14] != '3' && data[14] != '0')) return -3; //so são aceites blocos de meia hora certos
    if (data[11] > '1' || (data[11] == '0' && data[12] < '8') || (data[11] == '1' && data[12] > '7')) return -4; //estacao de serviço só funciona das 8:00 às 18:00
    char * tempo_actual = time_to_string();
    if (time_difference(tempo_actual, data) > 0) {
        free(tempo_actual);
        return -5;
    }
    if (time_difference(tempo_actual, data) < -31536000) {
        free(tempo_actual);
        return -6;
    }
    free(tempo_actual);
    return 0;
}

int check_data_2(char * data, int tempo) {
    time_t now;
    time(&now);
    struct tm time1 = parse_time_to_struct(data, &now);
    mktime(&time1);
    if ((time1.tm_wday == 6 && time1.tm_hour >= 13) || (time1.tm_wday == 6 && time1.tm_hour == 12 && time1.tm_min == 30 && tempo == 60) || time1.tm_wday == 0) return -1;
    return 0;
}

int check_last_block(char * data, int tempo) {
    if (data[11] == '1' && data[12] == '7' && data[14] == '3' && tempo == 60) return -1; //manutencao não é possivel no ultimo bloco de meia hora
    return 0;
}

int ano_bissexto(int ano) {
	int y = ano;
	if (ano %100 == 0) y=ano/100;
	if (y%4 == 0) return 0;
	else return 1;
}

int check_data_bounds(char * data) {
    char * timex = (char*)malloc(17*sizeof(char)); //strtok only working with this
    strcpy(timex, data);
    char * temp = strtok(timex, "/ ");
    int dia = atoi(temp);
    temp = strtok(NULL, "/ ");
    int mes = atoi(temp);
    temp = strtok(NULL, "/ ");
    int ano = atoi(temp);
    free(timex);
	if (ano < 0 || mes > 12 || mes < 1 || dia > 31 || dia < 0) return -1;
	if (((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia > 30) || (dia > 29 && mes == 2)) return -1;
	if (mes == 2 && dia > 28 && ano_bissexto(ano) == 0) return -1;
	return 0;
}

void AddTime(int tempo, struct tm * date) {
    date->tm_sec += 60*tempo;
    mktime(date);
}

void imprime_data_conclusao(char * data, int tempo) {
    time_t now;
    time(&now);
    struct tm time1 = parse_time_to_struct(data,&now);
    time1.tm_sec = 0;
    mktime(&time1);
    AddTime(tempo, &time1);
    if (time1.tm_isdst == 0) time1.tm_hour += 1;
    char * data_temp = (char *)malloc(20*sizeof(char));
    data_temp[0] = '\0';
    char buffer[18];
    if (time1.tm_mday < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d/",time1.tm_mday);
    strcat(data_temp,buffer);
    if (time1.tm_mon+1 < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d/",time1.tm_mon+1);
    strcat(data_temp,buffer);
    sprintf(buffer,"%d ",time1.tm_year+1900);
    strcat(data_temp,buffer);
    if (time1.tm_hour < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d:",time1.tm_hour);
    strcat(data_temp,buffer);
    if (time1.tm_min < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d",time1.tm_min);
    strcat(data_temp,buffer);
    printf("%s\n",data_temp);
    free(data_temp);
}

void beginning_of_day(char * data, char * data_temp) { //must be freed after call
    time_t now;
    time(&now);
    struct tm time1 = parse_time_to_struct(data,&now);
    time1.tm_sec = 0;
    mktime(&time1);
    time1.tm_hour = 8;
    time1.tm_min = 0;
    data_temp[0] = '\0';
    char buffer[18];
    if (time1.tm_mday < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d/",time1.tm_mday);
    strcat(data_temp,buffer);
    if (time1.tm_mon+1 < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d/",time1.tm_mon+1);
    strcat(data_temp,buffer);
    sprintf(buffer,"%d ",time1.tm_year+1900);
    strcat(data_temp,buffer);
    if (time1.tm_hour < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d:",time1.tm_hour);
    strcat(data_temp,buffer);
    if (time1.tm_min < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d",time1.tm_min);
    strcat(data_temp,buffer);
}

void last_block_of_day(char * data, char * data_temp) { //must be freed after call
    time_t now;
    time(&now);
    struct tm time1 = parse_time_to_struct(data,&now);
    time1.tm_sec = 0;
    mktime(&time1);
    time1.tm_hour = 18;
    time1.tm_min = 0;
    data_temp[0] = '\0';
    char buffer[18];
    if (time1.tm_mday < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d/",time1.tm_mday);
    strcat(data_temp,buffer);
    if (time1.tm_mon+1 < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d/",time1.tm_mon+1);
    strcat(data_temp,buffer);
    sprintf(buffer,"%d ",time1.tm_year+1900);
    strcat(data_temp,buffer);
    if (time1.tm_hour < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d:",time1.tm_hour);
    strcat(data_temp,buffer);
    if (time1.tm_min < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d",time1.tm_min);
    strcat(data_temp,buffer);
}

void next_block(char * data, char * data_temp) {
    time_t now;
    time(&now);
    struct tm time1 = parse_time_to_struct(data,&now);
    time1.tm_sec = 0;
    mktime(&time1);
    AddTime(30, &time1);
    if (time1.tm_isdst == 0) time1.tm_hour += 1;
    data_temp[0] = '\0';
    char buffer[18];
    if (time1.tm_mday < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d/",time1.tm_mday);
    strcat(data_temp,buffer);
    if (time1.tm_mon+1 < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d/",time1.tm_mon+1);
    strcat(data_temp,buffer);
    sprintf(buffer,"%d ",time1.tm_year+1900);
    strcat(data_temp,buffer);
    if (time1.tm_hour < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d:",time1.tm_hour);
    strcat(data_temp,buffer);
    if (time1.tm_min < 10) strcat(data_temp,"0");
    sprintf(buffer,"%d",time1.tm_min);
    strcat(data_temp,buffer);
}









//
//  estacao_servico.c
//  projecto_ppp
//
//  Created by Pedro Quitério on 28/04/14.
//  Copyright (c) 2014 pedromql. All rights reserved.
//

#include <stdio.h>
#include "estacao_servico.h"

//
//Funcoes de reserva
//


/* 
*   Aloca reserva e inicializa os valores a NULL/0
*/
Reserva * nova_reserva() {
    Reserva * reserva = (Reserva *)malloc(sizeof(Reserva));
    reserva->anterior = NULL;
    reserva->cliente = NULL;
    reserva->data[0] = '\0';
    reserva->tempo = 0;
    reserva->seguinte = NULL;
    return reserva;
}


/* 
*   Função para criar nova reserva. Deve receber um ponteiro para a primeira reserva da lista, que já tem que estar alocado
*   e o ponteiro para o Cliente que pretende associar.
*   Recebe a data a atribuir à reserva e o tempo de duração (30 ou 60).
*   O primeiro ponteiro da lista nunca é alterado!
*/
void cria_reserva(Reserva * reservas, Cliente * cliente, char * data, int tempo) {
    Reserva * reserva_temp;
    if (reservas->tempo != 0) {                                         //caso já existe uma reserva feita
        if (time_difference(reservas->data, data) > 0) {                //caso a primeira reserva seja posterior à reserva a adicionar coloca os dados da primeira reserva para uma segunda reserva e copia os dados 
            reserva_temp = reservas->seguinte;                          //guarda ponteiro para a segunda reserva
            reservas->seguinte = nova_reserva();                        //Cria uma reserva com os campos vazios;
            reservas->seguinte->seguinte = reserva_temp;                //A nova reserva (segunda) guarda o ponteiro da antiga segunda reserva 
            if (reservas->seguinte->seguinte != NULL) reservas->seguinte->seguinte->anterior = reservas->seguinte;      //caso a antiga segunda reserva não seja NULL diz-lhe qual é o seu novo ponteiro anterior (que contém a informação da antiga primeira reserva)
            /*
            *       Copia a informação da antiga primeira reserva para a segunda posição da lista
            */
            reservas->seguinte->anterior = reservas;                    
            reservas->seguinte->cliente = reservas->cliente;
            strcpy(reservas->seguinte->data,reservas->data);
            reservas->seguinte->tempo = reservas->tempo;
            /*
            *       Copia a informação da nova reserva para o primeiro ponteiro e termina
            */
            reservas->cliente = cliente;
            strcpy(reservas->data,data);
            reservas->tempo = tempo;
            return;
        }
        /*
        *       Avançar nas reserva até encontrar uma que seja posterior
        */
        while (reservas->seguinte != NULL) {
            reservas = reservas->seguinte;
            if (time_difference(reservas->data, data) > 0) {            //quando encontra um elemento posterior
                /*
                *       Caso o elemento seguinte NULL aloca memoria para o mesmo
                *       Copia as informações da reserva existente para o novo seguinte
                *       Coloca as informações da nova reserva no ponteiro que já existia e termina
                */
                if (reservas->seguinte == NULL) {
                    reservas->seguinte = nova_reserva();
                    reservas->seguinte->anterior = reservas;
                    reservas->seguinte->cliente = reservas->cliente;
                    strcpy(reservas->seguinte->data,reservas->data);
                    reservas->seguinte->tempo = reservas->tempo;
                    reservas->cliente = cliente;
                    strcpy(reservas->data,data);
                    reservas->tempo = tempo;
                    return;
                }
                /*
                *       Caso o elemento seguinte exista guarda o seu endereço numa variável temporária e cria um novo seguinte
                *       Copia as informações da reserva existente para o novo seguinte
                *       O novo seguinte passa a ter como seguinte o endereço guardado na variavel temporária
                *       Na reserva em que estamos colocam-se os dados da nova reserva e termina
                */
                else {
                    reserva_temp = reservas->seguinte;
                    reservas->seguinte = nova_reserva();
                    reservas->seguinte->anterior = reservas;
                    reservas->seguinte->seguinte = reserva_temp;
                    reserva_temp->anterior = reservas->seguinte;
                    reservas->seguinte->cliente = reservas->cliente;
                    strcpy(reservas->seguinte->data,reservas->data);
                    reservas->seguinte->tempo = reservas->tempo;
                    reservas->cliente = cliente;
                    strcpy(reservas->data,data);
                    reservas->tempo = tempo;
                    return;
                }
            }
        } 
        /*
        *       No caso de a nova reserva ser posterior às reservas existente aloca memória para a própria e copia os dados
        */
        reservas->seguinte = nova_reserva();
        reservas->seguinte->anterior = reservas;
        reservas = reservas->seguinte;
        reservas->cliente = cliente;
        strcpy(reservas->data,data);
        reservas->tempo = tempo;
    }
    /*
    *       Caso em que não há reservas
    *       Apenas copia os dados para o primeiro endereço
    */
    else {
        reservas->cliente = cliente;
        strcpy(reservas->data,data);
        reservas->tempo = tempo;
    }
}

/*
*       Função para encontrar reserva com a mesma data ou que se sobreponham (no caso das manutenções de 60 minutos)
*       Retorna o enderenço para a sobreposição ou caso não haja sobreposição retorna NULL
*/
Reserva * procura_reserva_sobreposta(Reserva * reservas, char * data, int tempo) {
    if (reservas->tempo == 0) return NULL;                                                          //Caso não existam reservas retorna NULL
    else {
        long difference = time_difference(reservas->data, data);                                    //Guarda o tempo de diferença entre a reserva encontrada e a recebida nos argumentos
        /*
        *       Retorna caso a hora da reserva seja a mesma
        *       Retorna caso comece uma manutenção na meia hora anterior
        *       Retorna caso seja para reservar uma manutenção e existe alguma reserva a começar na meia hora seguinte
        */
        if (difference == 0 || (difference == -1800 && reservas->tempo == 60) || (difference == 1800 && tempo == 60)) return reservas;
        while (reservas->seguinte != NULL) { //avanca nas reservas
            reservas = reservas->seguinte;
            difference = time_difference(reservas->data, data);
            if (difference == 0 || (difference == -1800 && reservas->tempo == 60) || (difference == 1800 && tempo == 60)) return reservas;
        }
    }
    return NULL;
}


/*
*       Simplesmente avança nas reservas até encontrar uma com a mesma data e retorna
*       Caso não encontre retorna NULL
*       Uso strcmp em vez de time_difference porque é provavelmente mais rápido e eficiente
*/
Reserva * procura_reserva_data(Reserva * reservas, char * data) { //devo adicionar == tempo?!
    if (reservas->tempo == 0) return NULL;
    if (strcmp(reservas->data,data) == 0) return reservas;
    while (reservas->seguinte != NULL) {
        reservas = reservas->seguinte;
        if (strcmp(reservas->data,data) == 0) return reservas; 
    }
    return NULL;
}


/*
*       Encontra a reserva e cancela-a fazendo free ao endereço de memória e ligando a reserva anterior com a seguinte
*       Retorna o ponteiro para a nova primeira reserva (apenas afecta caso existam 2 ou mais reservas e a primeira reserva seja cancelada
*/
Reserva * cancela_reserva(Reserva * reservas,Cliente * clientes, char * data) {
    Reserva * primeira_reserva = reservas;                                                              //Guarda a primeira reserva para return
    if (reservas-> tempo == 0) return reservas;                                                         //Caso não existam reservas deixa como estava a retorna o ponteiro inicial
    /*
    *       Caso a primeira reserva seja a reserva a cancelar e existam mais reserva na lista
    *       Coloca o ponteiro anterior da segunda reserva a apontar para NULL
    *       Faz free da memória da reserva e retorna o endereço da segunda reserva que passa a ser a primeira
    */
    if ((reservas->cliente->numero_cliente == clientes->numero_cliente) && strcmp(reservas->data,data) == 0 && reservas->seguinte != NULL) {
        reservas->seguinte->anterior = NULL;
        primeira_reserva = reservas->seguinte;
        free(reservas);
        return primeira_reserva;
    }
    /*
    *       Caso a primeira reserva seja a reserva a cancelar e seja a única
    *       Simplesmente anula os campos e retorna o seu endereço
    */
    else if (strcmp(reservas->data,data) == 0 && reservas->seguinte == NULL) {
        reservas->anterior = NULL;
        reservas->cliente = NULL;
        reservas->data[0] = '\0';
        reservas->tempo = 0;
        reservas->seguinte = NULL;
        return primeira_reserva;
    }
    while (reservas->seguinte != NULL) {                                                //avança nas reserva até encontrar a reserva a cancelar
        reservas = reservas->seguinte;
        if (reservas->seguinte == NULL) break;
        /*
        *       Caso a reserva a cancelar seja uma reserva do meio faz free da mesma
        *       Liga os ponteiros entre as reservas que tinha antes e a que tinha depois e retorn a primeira reserva
        */
        if (strcmp(reservas->data,data) == 0) {
            reservas->anterior->seguinte = reservas->seguinte;
            reservas->seguinte->anterior = reservas->anterior;
            free(reservas);
            return primeira_reserva;
        }
    }
    /*
    *       Caso seja a ultima reserva a cancelar faz free da mesma
    *       Coloca o ponteiro seguinte da penultima reserva a apontar para NULL
    */
    if (strcmp(reservas->data,data) == 0 && reservas != primeira_reserva) {
        reservas->anterior->seguinte = NULL;
        free(reservas);
    }
    return primeira_reserva;
}


/*
*       Procura reserva anterior à data actual e returna a mesma
*       Baseada na função procura reserva
*/
Reserva * procura_reserva_antiga(Reserva * reservas, char * data) {
    if (reservas->tempo == 0) return NULL;
    if (time_difference(data, reservas->data) > 0) return reservas;
    while (reservas->seguinte != NULL) {
        reservas = reservas->seguinte;
        if (time_difference(data, reservas->data) > 0) return reservas; 
    }
    return NULL;
}


/*
*       Procura reservas antigas com recurso à função procura_reservas_antigas e cancela as mesmas
*/
Reserva * cancela_reservas_antigas(Reserva * reservas, Cliente * clientes) {
    char * data_buffer = time_to_string();
    Reserva * reserva_para_cancelar = procura_reserva_antiga(reservas, data_buffer);
    while (reserva_para_cancelar != NULL) {
        reservas = cancela_reserva(reservas, reserva_para_cancelar->cliente, reserva_para_cancelar->data);
        reserva_para_cancelar = procura_reserva_antiga(reservas, data_buffer);
    }
    free(data_buffer);
    return reservas;
}


/*
*       Imprime reservas e pre-reservas por ordem cronológica
*       Opcao para imprimir apenas um dos tipos ou ambos
*/
void imprime_reservas(Reserva * reservas, Pre_reserva * pre_reservas, int opcao) {
    if (reservas->tempo == 0) {
        reservas = NULL;
        opcao = 1;
    }
    if (pre_reservas->tempo == 0) {
        pre_reservas = NULL;
        opcao = 0;
    }
    if (opcao == 0) {
        while (reservas != NULL) {
            printf("\nCliente : %s\nN.º     : %d\nNIF     : %d\nData    : %s\nTempo   : %d minutos\n",reservas->cliente->nome,reservas->cliente->numero_cliente,reservas->cliente->nif,reservas->data,reservas->tempo);
            reservas = reservas->seguinte;
        }
    }
    if (opcao == 1) {
        while (pre_reservas != NULL) {
            printf("\nCliente       : %s\nN.º           : %d\nNIF           : %d\nData          : %s\nData Inserção : %s\nTempo         : %d minutos\n",pre_reservas->cliente->nome,pre_reservas->cliente->numero_cliente,pre_reservas->cliente->nif,pre_reservas->data,pre_reservas->data_insercao,pre_reservas->tempo);
            pre_reservas = pre_reservas->seguinte;
        }
        
    }
    /*
    *       Neste caso é necessario verificar se acontece primeiro a reserva ou a pre-reserva
    *       Imprime a que acontecer primeiro ou em caso de igualdade a reserva
    */
    if (opcao == 2) {
        while (reservas != NULL || pre_reservas != NULL) {
            if (pre_reservas == NULL || pre_reservas->tempo == 0) {
                if (reservas != NULL) {
                    printf("\nCliente : %s\nN.º     : %d\nNIF     : %d\nData    : %s\nTempo   : %d minutos\n",reservas->cliente->nome,reservas->cliente->numero_cliente,reservas->cliente->nif,reservas->data,reservas->tempo);
                    reservas = reservas->seguinte;
                }
            }
            else if (reservas == NULL || reservas->tempo == 0) {
                printf("\nCliente       : %s\nN.º           : %d\nNIF           : %d\nData          : %s\nData Inserção : %s\nTempo         : %d minutos\n",pre_reservas->cliente->nome,pre_reservas->cliente->numero_cliente,pre_reservas->cliente->nif,pre_reservas->data,pre_reservas->data_insercao,pre_reservas->tempo);
                pre_reservas = pre_reservas->seguinte;
            }
            else if (time_difference(reservas->data, pre_reservas->data) <= 0) {
                printf("\nCliente : %s\nN.º     : %d\nNIF     : %d\nData    : %s\nTempo   : %d minutos\n",reservas->cliente->nome,reservas->cliente->numero_cliente,reservas->cliente->nif,reservas->data,reservas->tempo);
                reservas = reservas->seguinte;
            }
            else if (time_difference(reservas->data, pre_reservas->data) > 0) {
                printf("\nCliente       : %s\nN.º           : %d\nNIF           : %d\nData          : %s\nData Inserção : %s\nTempo         : %d minutos\n",pre_reservas->cliente->nome,pre_reservas->cliente->numero_cliente,pre_reservas->cliente->nif,pre_reservas->data,pre_reservas->data_insercao,pre_reservas->tempo);
                pre_reservas = pre_reservas->seguinte;
            }
        }
    }
}

/*
*       Imprime os blocos de horas disponíveis para reserva num determinado dia
*/
void available_block(Reserva * reservas, char * data, int tempo) {
    char * data_temp_1 = (char *)malloc(18*sizeof(char));
    char * last = (char *)malloc(18*sizeof(char));
    beginning_of_day(data, data_temp_1); //inicio do dia
    last_block_of_day(data_temp_1,last);
    int i = 0;
    /*
    *       Começa no inicio do dia a testar e termina no ultimo bloco
    */
    do {
        /*
        *       Enquanto houver uma resersa sobreposta avanço um bloco no data_temp_1
        */
        while (procura_reserva_sobreposta(reservas, data_temp_1, 30) != NULL && strcmp(last, data_temp_1) != 0) {
            next_block(data_temp_1, data_temp_1);
        }
        if (strcmp(last, data_temp_1) == 0 && i == 0) printf("Não há períodos disponíveis no dia pretendido!\n");                   //Caso chega ao final no primeiro ciclo informa que não há blocos disponíveis para esse dia
        if (strcmp(last, data_temp_1) == 0) break;
        if (i == 0) printf("Nesse dia, a estação de serviço encontra-se livre nos seguintes períodos: \n");
        i++;
        printf("[%c%c:%c%c ,",data_temp_1[11],data_temp_1[12],data_temp_1[14],data_temp_1[15]);                                     //Imprime o inicio do bloco livre
        while (procura_reserva_sobreposta(reservas, data_temp_1, 30) == NULL && strcmp(last, data_temp_1) != 0) {                   //Avança até encontrar a sobreposição seguinte
            next_block(data_temp_1, data_temp_1);
        }
        printf(" %c%c:%c%c[\n",data_temp_1[11],data_temp_1[12],data_temp_1[14],data_temp_1[15]);                                    //Imprime o fim do bloco total livre
    } while (strcmp(last, data_temp_1) != 0);
}

/*
*       Destrói lista de reservas
*/
void free_reservas(Reserva * reservas) {
    Reserva * reserva_temp = reservas;
    while (reservas != NULL) {
        reserva_temp = reservas;
        if (reservas->seguinte != NULL) {
            reservas = reservas->seguinte;
            free(reserva_temp);
        }
        else {
            free(reservas);
            break;
        }
    }
}

//
//Funcoes de pre-reserva
//

/*
*       Alocar no pre_reserva e inicial os seus elementos a NULL/0
*/
Pre_reserva * nova_pre_reserva() {
    Pre_reserva * pre_reserva = (Pre_reserva *)malloc(sizeof(Pre_reserva));
    pre_reserva->anterior = NULL;
    pre_reserva->cliente = NULL;
    pre_reserva->data[0] = '\0';
    pre_reserva->data_insercao[0] = '\0';
    pre_reserva->tempo = 0;
    pre_reserva->seguinte = NULL;
    return pre_reserva;
}

/*
*       Função para criar nova pre reserva. Deve receber um ponteiro para a primeira pre reserva da lista, que já tem de estar alocado
*       e o ponteiro para o cliente que associar
*       Recebe a data a atribuir a pre reserva e o tempo de duração (30 ou 60)
*       Coloca também a data de inserção da reserva
*       O primeiro ponteiro nunca é alterado
*/
void cria_pre_reserva(Pre_reserva * pre_reservas, Cliente * cliente, char * data, int tempo) {
    Pre_reserva * pre_reserva_temp;
    if (pre_reservas->tempo != 0) {                                                                             //caso já exista uma pre reserva
        /*
        *       Caso a primeira pre reserva seja posterior à reserva a inserir cria uma pre-reserva para ficar em segundo, copia os dados da primeira pre reserva para a nova reserva e "avança o segundo ponteiro para terceiro"
        *       Os dados da nova reserva são copiados para a reserva inicial 
        */
        if (time_difference(pre_reservas->data, data) > 0) {                                                    
            pre_reserva_temp = pre_reservas->seguinte;
            pre_reservas->seguinte = nova_pre_reserva();
            pre_reservas->seguinte->seguinte = pre_reserva_temp;
            if (pre_reservas->seguinte->seguinte != NULL) pre_reservas->seguinte->seguinte->anterior = pre_reservas->seguinte;
            pre_reservas->seguinte->anterior = pre_reservas;
            pre_reservas->seguinte->cliente = pre_reservas->cliente;
            strcpy(pre_reservas->seguinte->data,pre_reservas->data);
            strcpy(pre_reservas->seguinte->data_insercao,pre_reservas->data_insercao);
            pre_reservas->seguinte->tempo = pre_reservas->tempo;
            pre_reservas->cliente = cliente;
            strcpy(pre_reservas->data,data);
            char * data_buffer = time_to_string();
            strcpy(pre_reservas->data_insercao, data_buffer);
            free(data_buffer);
            pre_reservas->tempo = tempo;
            return;
        }
        /*
        *       Avança nas pre reservas ate encontrar uma que seja posterior
        */
        while (pre_reservas->seguinte != NULL) { 
            pre_reservas = pre_reservas->seguinte;
            if (time_difference(pre_reservas->data, data) > 0) {                                        //quando encontra um elemento posterior
                /*
                *       Caso o elemento seguinte seja NULL aloca memoria para o mesmo
                *       Copia as informações da reserva em que nos encontramos para o novo ponteiro
                *       Coloca as informações da nova reserva no ponteiro que já existia e termina
                */            
                if (pre_reservas->seguinte == NULL) {
                    pre_reservas->seguinte = nova_pre_reserva();
                    pre_reservas->seguinte->anterior = pre_reservas;
                    pre_reservas->seguinte->cliente = pre_reservas->cliente;
                    strcpy(pre_reservas->seguinte->data,pre_reservas->data);
                    strcpy(pre_reservas->seguinte->data_insercao,pre_reservas->data_insercao);
                    pre_reservas->seguinte->tempo = pre_reservas->tempo;
                    pre_reservas->cliente = cliente;
                    strcpy(pre_reservas->data,data);
                    char * data_buffer = time_to_string();
                    strcpy(pre_reservas->data_insercao, data_buffer);
                    free(data_buffer);
                    pre_reservas->tempo = tempo;
                    return;
                }
                /*
                *       Caso o elemento seguinte exista, guarda o seu ponteiro numa variavel temporario e cria um novo nó seguinte
                *       Copia as informações da reserva actual para o novo ponteiro
                *       O novo ponteiro passa a ter como seguinte o ponteiro que estava guardado na variável temporária
                *       Na reservas em que nos encontramos colocam-se os dados  da nova reserva e termina
                */
                else {
                    pre_reserva_temp = pre_reservas->seguinte;
                    pre_reservas->seguinte = nova_pre_reserva();
                    pre_reservas->seguinte->anterior = pre_reservas;
                    pre_reservas->seguinte->seguinte = pre_reserva_temp;
                    pre_reserva_temp->anterior = pre_reservas->seguinte;
                    pre_reservas->seguinte->cliente = pre_reservas->cliente;
                    strcpy(pre_reservas->seguinte->data,pre_reservas->data);
                    strcpy(pre_reservas->seguinte->data_insercao,pre_reservas->data_insercao);
                    pre_reservas->seguinte->tempo = pre_reservas->tempo;
                    pre_reservas->cliente = cliente;
                    strcpy(pre_reservas->data,data);
                    char * data_buffer = time_to_string();
                    strcpy(pre_reservas->data_insercao, data_buffer);
                    free(data_buffer);
                    pre_reservas->tempo = tempo;
                    return;
                }
            }
        }
        /*
        *   No caso de a pre reserva ser posterior às pre reservas existentes aloca memória para a própria e copia os dados
        */
        pre_reservas->seguinte = nova_pre_reserva();
        pre_reservas->seguinte->anterior = pre_reservas;
        pre_reservas = pre_reservas->seguinte;
        pre_reservas->cliente = cliente;
        strcpy(pre_reservas->data,data);
        char * data_buffer = time_to_string();
        strcpy(pre_reservas->data_insercao, data_buffer);
        free(data_buffer);
        pre_reservas->tempo = tempo;
    }
    /*
    *       No caso em que não há pre reservas apenas copia os dados para o endereço de memória já alocado
    */
    else {
        pre_reservas->cliente = cliente;
        strcpy(pre_reservas->data,data);
        char * data_buffer = time_to_string();
        strcpy(pre_reservas->data_insercao, data_buffer);
        free(data_buffer);
        pre_reservas->tempo = tempo;
    }
}

/*
*       Função igual à cria pre reserva apenas com a diferença que lê o tempo de inserção do ficheiro ao invés de colocar o tempo de sistema
*/
void add_pre_reserva_from_file(Pre_reserva * pre_reservas,Cliente * cliente, int num_cliente, char * data, char * data_insercao, int tempo) {
    procura_numero_cliente(cliente, num_cliente);
    Pre_reserva * pre_reserva_temp;
    if (pre_reservas->tempo != 0) {
        if (time_difference(pre_reservas->data, data) > 0) {
            pre_reserva_temp = pre_reservas->seguinte;
            pre_reservas->seguinte = nova_pre_reserva();
            pre_reservas->seguinte->seguinte = pre_reserva_temp;
            if (pre_reservas->seguinte->seguinte != NULL) pre_reservas->seguinte->seguinte->anterior = pre_reservas->seguinte;
            pre_reservas->seguinte->anterior = pre_reservas;
            pre_reservas->seguinte->cliente = pre_reservas->cliente;
            strcpy(pre_reservas->seguinte->data,pre_reservas->data);
            strcpy(pre_reservas->seguinte->data_insercao,pre_reservas->data_insercao);
            pre_reservas->seguinte->tempo = pre_reservas->tempo;
            pre_reservas->cliente = cliente;
            strcpy(pre_reservas->data,data);
            strcpy(pre_reservas->data_insercao, data_insercao);
            pre_reservas->tempo = tempo;
            return;
        }
        while (pre_reservas->seguinte != NULL) {
            pre_reservas = pre_reservas->seguinte;
            if (time_difference(pre_reservas->data, data) > 0) {
                if (pre_reservas->seguinte == NULL) {
                    pre_reservas->seguinte = nova_pre_reserva();
                    pre_reservas->seguinte->anterior = pre_reservas;
                    pre_reservas->seguinte->cliente = pre_reservas->cliente;
                    strcpy(pre_reservas->seguinte->data,pre_reservas->data);
                    strcpy(pre_reservas->seguinte->data_insercao,pre_reservas->data_insercao);
                    pre_reservas->seguinte->tempo = pre_reservas->tempo;
                    pre_reservas->cliente = cliente;
                    strcpy(pre_reservas->data,data);
                    strcpy(pre_reservas->data_insercao, data_insercao);
                    pre_reservas->tempo = tempo;
                    return;
                }
                else {
                    pre_reserva_temp = pre_reservas->seguinte;
                    pre_reservas->seguinte = nova_pre_reserva();
                    pre_reservas->seguinte->anterior = pre_reservas;
                    pre_reservas->seguinte->seguinte = pre_reserva_temp;
                    pre_reserva_temp->anterior = pre_reservas->seguinte;
                    pre_reservas->seguinte->cliente = pre_reservas->cliente;
                    strcpy(pre_reservas->seguinte->data,pre_reservas->data);
                    strcpy(pre_reservas->seguinte->data_insercao,pre_reservas->data_insercao);
                    pre_reservas->seguinte->tempo = pre_reservas->tempo;
                    pre_reservas->cliente = cliente;
                    strcpy(pre_reservas->data,data);
                    strcpy(pre_reservas->data_insercao, data_insercao);
                    pre_reservas->tempo = tempo;
                    return;
                }
            }
        }
        pre_reservas->seguinte = nova_pre_reserva();
        pre_reservas->seguinte->anterior = pre_reservas;
        pre_reservas = pre_reservas->seguinte;
        pre_reservas->cliente = cliente;
        strcpy(pre_reservas->data,data);
        strcpy(pre_reservas->data_insercao, data_insercao);
        pre_reservas->tempo = tempo;
    }
    else {
        pre_reservas->cliente = cliente;
        strcpy(pre_reservas->data,data);
        strcpy(pre_reservas->data_insercao, data_insercao);
        pre_reservas->tempo = tempo;
    }
}

/*
*       Destroi pre reservas
*/
void free_pre_reservas(Pre_reserva * pre_reservas) {
    Pre_reserva * pre_reserva_temp;
    while (pre_reservas != NULL) {
        pre_reserva_temp = pre_reservas;
        if (pre_reservas->seguinte != NULL) {
            pre_reservas = pre_reservas->seguinte;
            free(pre_reserva_temp);
        }
        else {
            free(pre_reservas);
            break;
        }
    }
}



/*
*       Encontra a reserva e cancela fazendo free ao endereço de memória e ligando a reserva seguinte à anterior
*/
Pre_reserva * cancela_pre_reserva(Pre_reserva * pre_reservas, Cliente * cliente, char * data, char * data_insercao, int tempo) {
    Pre_reserva * primeira_pre_reserva = pre_reservas;                                              //Guarda a primeira pre reserva
    if (pre_reservas-> tempo == 0) return pre_reservas;                                             //Caso não existam reservas devolve a primeira pre reserva
    /*
    *       Caso a primeira pre reserva seja para cancelar
    *       Coloca o ponteiro anterior da segunda a apontar para NULL
    *       Faz free da memoria da primeira e retorna a seunga pre reserva que passa a ser a primeira
    */
    if (pre_reservas->cliente->numero_cliente == cliente->numero_cliente && strcmp(pre_reservas->data,data) == 0 && strcmp(pre_reservas->data_insercao,data_insercao) == 0 && pre_reservas->tempo == tempo && pre_reservas->seguinte != NULL) {
        pre_reservas->seguinte->anterior = NULL;
        primeira_pre_reserva = pre_reservas->seguinte;
        free(pre_reservas);
        return primeira_pre_reserva;
    }
    /*
    *       Caso seja para cancelar a primeira pre reserva e esta seja unica apenas anula os campos do seu endereço
    */
    else if (strcmp(pre_reservas->data,data) == 0 && pre_reservas->seguinte == NULL) {
        pre_reservas->anterior = NULL;
        pre_reservas->cliente = NULL;
        pre_reservas->data[0] = '\0';
        pre_reservas->data_insercao[0] = '\0';
        pre_reservas->tempo = 0;
        pre_reservas->seguinte = NULL;
        return primeira_pre_reserva;
    }
    while (pre_reservas->seguinte != NULL) {                                                // avança nas pre reservas até encontra a pre reserva a cancelar
        pre_reservas = pre_reservas->seguinte;
        if (pre_reservas->seguinte == NULL) break;
        /*
        *       Caso a reserva a cancelar seja uma reserva do meio faz free da mesma e liga os ponteiros entre a reserva anterior e a seguinte
        */
        if (strcmp(pre_reservas->data,data) == 0) {
            pre_reservas->anterior->seguinte = pre_reservas->seguinte;
            pre_reservas->seguinte->anterior = pre_reservas->anterior;
            free(pre_reservas);
            return primeira_pre_reserva;
        }
    }
    /*
    *   Caso a pre reserva a cancelar seja a ultima faz free da mesma e coloca o ponteiro seguinte da penultima a apontar para NULL
    */
    if (strcmp(pre_reservas->data,data) == 0) {
        pre_reservas->anterior->seguinte = NULL;
        free(pre_reservas);
    }
    return primeira_pre_reserva;
}


/*
*       Percorre as pre reservas até encontrar uma pre reserva anterior à data actual e retorna-a
*/
Pre_reserva * procura_pre_reserva_antiga(Pre_reserva * pre_reservas, char * data) {
    if (pre_reservas->tempo == 0) return NULL;
    if (time_difference(data, pre_reservas->data) > 0) return pre_reservas;
    while (pre_reservas->seguinte != NULL) {
        pre_reservas = pre_reservas->seguinte;
        if (time_difference(data, pre_reservas->data) > 0) return pre_reservas;
    }
    return NULL;
}

/*
*       Cancela pre reservas anteriores à data actual
*/
Pre_reserva * cancela_pre_reservas_antigas(Pre_reserva * pre_reservas, Cliente * clientes) {
    char * data_buffer = time_to_string();
    Pre_reserva * pre_reserva_para_cancelar = procura_pre_reserva_antiga(pre_reservas, data_buffer);
    while (pre_reserva_para_cancelar != NULL) {
        pre_reservas = cancela_pre_reserva(pre_reservas, pre_reserva_para_cancelar->cliente, pre_reserva_para_cancelar->data, pre_reserva_para_cancelar->data_insercao, pre_reserva_para_cancelar->tempo);
        pre_reserva_para_cancelar = procura_pre_reserva_antiga(pre_reservas, data_buffer);
    }
    free(data_buffer);
    return pre_reservas;
}


/*
*       Para cada pre reserva verifica se já há uma reserva feita para essa data
*       Caso não existe, promove-a
*/
Pre_reserva * check_pre_reserva(Reserva * reservas, Pre_reserva * pre_reservas) {
    Pre_reserva * primeira_pre_reserva = pre_reservas;
    while (pre_reservas != NULL && pre_reservas->tempo != 0) {
        if (procura_reserva_sobreposta(reservas, pre_reservas->data, pre_reservas->tempo) == NULL) { //falta verificar se e a pre reserva feita ha mais tempo
            cria_reserva(reservas, pre_reservas->cliente, pre_reservas->data, pre_reservas->tempo);
            //while (pre_reservas->anterior != NULL) pre_reservas = pre_reservas->anterior;
            primeira_pre_reserva = cancela_pre_reserva(primeira_pre_reserva, pre_reservas->cliente, pre_reservas->data, pre_reservas->data_insercao, pre_reservas->tempo);
            return primeira_pre_reserva;
        }
        pre_reservas = pre_reservas->seguinte;
    }
    return primeira_pre_reserva;
}


/*
*       Deixou de ter utilidade na ultima versão
*/
Pre_reserva * procura_pre_reserva(Pre_reserva * pre_reservas, int num_cliente, char * data, char * data_insercao, int tempo) {
    if (pre_reservas->tempo == 0) return NULL;
    if (pre_reservas->cliente->numero_cliente == num_cliente && strcmp(pre_reservas->data,data) == 0 && strcmp(pre_reservas->data_insercao, data_insercao) == 0 && pre_reservas->tempo == tempo) {
        return pre_reservas;
    }
    while (pre_reservas->seguinte != NULL) {
        pre_reservas = pre_reservas->seguinte;
        if (pre_reservas->cliente->numero_cliente == num_cliente && strcmp(pre_reservas->data,data) == 0 && strcmp(pre_reservas->data_insercao, data_insercao) == 0 && pre_reservas->tempo == tempo) return pre_reservas; //podia usar a funcao time_difference() mas julgo que assim sera mais rapido
    }
    return NULL;
}


/*
*       Procura uma pre reserva com as caracteristicas do argumentos e devolve-a
*/
Pre_reserva * procura_pre_reserva_2(Pre_reserva * pre_reservas, int num_cliente, char * data, int tempo) {
    if (pre_reservas->tempo == 0) return NULL;
    if (pre_reservas->cliente->numero_cliente == num_cliente && strcmp(pre_reservas->data,data) == 0 && pre_reservas->tempo == tempo) {
        return pre_reservas;
    }
    while (pre_reservas->seguinte != NULL) {
        pre_reservas = pre_reservas->seguinte;
        if (pre_reservas->cliente->numero_cliente == num_cliente && strcmp(pre_reservas->data,data) == 0 && pre_reservas->tempo == tempo) return pre_reservas; //podia usar a funcao time_difference() mas julgo que assim sera mais rapido
    }
    return NULL;
}



//
//Funcoes de cliente
//


/*
*       Aloca memoria para o novo cliente e inicializa os seus campos NULL/0
*/
Cliente * novo_cliente() {
    Cliente * cliente = (Cliente *)malloc(sizeof(Cliente));
    cliente->nome[0] = '\0';
    cliente->numero_cliente = 0;
    cliente->nif = 0;
    return cliente;
}

/*
*       Avança até ao ultimo cliente, contando-os
*       E adiciona-o no final da lista
*/
int cria_cliente(Cliente * clientes, char * nome, int nif) {
    int i = 1;
    if (clientes->numero_cliente != 0) {
        while (clientes->seguinte != NULL) {
            clientes = clientes->seguinte;
            i++;
        }
        clientes->seguinte = novo_cliente();
        clientes->seguinte->anterior = clientes; 
        clientes = clientes->seguinte;
        i++;
    }
    clientes->seguinte = NULL;
    strcpy(clientes->nome,nome);
    clientes->numero_cliente = i;  
    clientes->nif = nif;
    return i;
}

/*
*       Igual à função cria cliente apenas com a diferença que o numero de cliente é lido do ficheiro
*/
void add_cliente_from_file(Cliente * clientes, char * nome, int nif, int num_cliente) {
    if (clientes->numero_cliente != 0) {
        while (clientes->seguinte != NULL) {
            clientes = clientes->seguinte;
        }
        clientes->seguinte = novo_cliente();
        clientes->seguinte->anterior = clientes; 
        clientes = clientes->seguinte;
    }
    clientes->seguinte = NULL;
    strcpy(clientes->nome,nome);
    clientes->numero_cliente = num_cliente;
    clientes->nif = nif;
}

/*
*       Avança nos clientes até encontrar um nif igual e devolve-o
*       Retorna NULL caso não encontre
*/
Cliente * procura_nif(Cliente * clientes, int nif) {
    if (clientes == NULL) return NULL;
    if (clientes->nif == nif) return clientes;
    while (clientes->seguinte != NULL) {
        clientes = clientes->seguinte;
        if (clientes->nif == nif) return clientes;
    }
    return NULL;
}

/*
*       Retorna o cliente com o numero de cliente pedido
*/
Cliente * procura_numero_cliente(Cliente * clientes, int numero_cliente) {
    if (clientes == NULL) return NULL;
    if (clientes->numero_cliente == numero_cliente) return clientes;
    while (clientes->seguinte != NULL) {
        clientes = clientes->seguinte;
        if (clientes->numero_cliente == numero_cliente) return clientes;
    }
    return NULL;
}

/*
*       Procura um cliente pelo seu nome. Apenas devolve a primeira ocorrencia
*/
Cliente * procura_cliente_nome(Cliente * clientes, char * nome) {
    if (clientes == NULL) return NULL;
    if (strcmp(clientes->nome, nome) == 0) return clientes;
    while (clientes->seguinte != NULL) {
        clientes = clientes->seguinte;
        if (strcmp(clientes->nome, nome) == 0) return clientes;
    }
    return NULL;
}

/*
*       Percorres as listas de reservas e pre reservas e devolve a quantidade total das mesmas associadas a um cliente específico
*/
int num_reservas_cliente(Reserva * reservas, Pre_reserva * pre_reservas, int num_cliente, int option) {
    int counter = 0;
    if (option == 1) { //check reservas
        while (reservas != NULL) {
            if (reservas->cliente->numero_cliente == num_cliente) counter++;
            reservas = reservas->seguinte;
        }
    }
    else { //check pre reservas
        while (pre_reservas != NULL) {
            if (pre_reservas->cliente->numero_cliente == num_cliente) counter++;
            pre_reservas = pre_reservas->seguinte;
        }
    }
    return counter;
}

/*
*       Avança para o final das reservas e pre reservas de forma a imprimir de modo por ordem inversa à cronológica
*/
void imprime_reservas_cliente(Cliente * cliente, Reserva * reservas, Pre_reserva * pre_reservas, int opcao) {
    
    if (reservas->tempo == 0) reservas = reservas->seguinte;
    else while (reservas->seguinte != NULL) reservas = reservas->seguinte;
    
    if (pre_reservas->tempo == 0) pre_reservas = pre_reservas->seguinte;
    else while (pre_reservas->seguinte != NULL) pre_reservas = pre_reservas->seguinte;
    
    if (opcao == 0) {
        while (reservas != NULL) {
            if (reservas->cliente == cliente) printf("\nCliente : %s\nData : %s\nTempo: %d minutos\n",reservas->cliente->nome,reservas->data,reservas->tempo);
            reservas = reservas->anterior;
        }
    }
    else if (opcao == 1) {
        while (pre_reservas != NULL) {
            if (pre_reservas->cliente == cliente) printf("\nCliente : %s\nData : %s\nData Inserção : %s\nTempo: %d minutos\n",pre_reservas->cliente->nome,pre_reservas->data,pre_reservas->data_insercao,pre_reservas->tempo);
            pre_reservas = pre_reservas->anterior;
        }
        
    }
    else if (opcao == 2) {
        while (reservas != NULL && pre_reservas != NULL) {
            if (pre_reservas == NULL || pre_reservas->tempo == 0) {
                if (reservas->cliente == cliente) printf("\nCliente : %s\nData : %s\nTempo: %d minutos\n",reservas->cliente->nome,reservas->data,reservas->tempo);
                reservas = reservas->anterior;
            }
            else if (reservas == NULL || pre_reservas->tempo == 0) {
                if (pre_reservas->cliente == cliente) printf("\nCliente : %s\nData : %s\nData Inserção : %s\nTempo: %d minutos\n",pre_reservas->cliente->nome,pre_reservas->data,pre_reservas->data_insercao,pre_reservas->tempo);
                pre_reservas = pre_reservas->anterior;
            }
            else if (time_difference(reservas->data, pre_reservas->data) >= 0) {
                if (reservas->cliente == cliente) printf("\nCliente : %s\nData : %s\nTempo: %d minutos\n",reservas->cliente->nome,reservas->data,reservas->tempo);
                reservas = reservas->anterior;
            }
            else if (time_difference(reservas->data, pre_reservas->data) < 0) {
                if (pre_reservas->cliente == cliente) printf("\nCliente : %s\nData : %s\nData Inserção : %s\nTempo: %d minutos\n",pre_reservas->cliente->nome,pre_reservas->data,pre_reservas->data_insercao,pre_reservas->tempo);
                pre_reservas = pre_reservas->anterior;
            }
        }
    }
}

/*
*       Imprime os dados de um cliente especifico
*/
void imprime_cliente_especifico(Cliente * cliente) {
    printf("Nome: %s\nNúmero de Cliente: %d\nNIF: %d\n",cliente->nome,cliente->numero_cliente,cliente->nif);
}

/*
*       Devolve o ultimo cliente da lista
*/
Cliente * last_client(Cliente * clientes) {
    while (clientes->seguinte != NULL) {
        clientes = clientes->seguinte;
    }
    return clientes;
}

/*
*       Destroi lista de clientes
*/
void free_clientes(Cliente * clientes) {
    Cliente * cliente_temp;
    while (clientes != NULL) {
        cliente_temp = clientes;
        if (clientes->seguinte != NULL) {
            clientes = clientes->seguinte;
            free(cliente_temp);
        }
        else {
            free(clientes);
            return;
        }
    }
}














